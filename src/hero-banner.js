import React, { Component } from 'react';
import './hero-banner.css';

class HeroBanner extends Component {

    render() {
        return (
            <div className="hero-banner">
                <div className="hero-container">
                    <h1 className="hero-header">StarFinder API</h1>
                </div>
            </div>
        )
    }
}

export default HeroBanner;
