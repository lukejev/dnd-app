import React, { Component } from 'react'
import './footer.css';

class Footer extends Component {

    handleClick(e) {
        console.log(e.target)
    }

    render() {
        return (
            <div>
                <div className='footer-container'>
                    <div onClick={this.handleClick} className ='footer-item footer-item-1'>Created By Lil Uzi Vert</div>
                    <div onClick={this.handleClick} className ='footer-item footer-item-2'>Donate</div>
                </div>
            </div>
        )
    }
}

export default Footer;
