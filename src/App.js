import React from 'react';
import HeroBanner from './hero-banner';
import NavBar from './nav-bar.js';
import ContentSection from './content-section';
import Footer from './footer';

function App() {
  return (
    <div className="App">
      <NavBar />
      <HeroBanner />
      <ContentSection />
      <Footer />
    </div>
  );
}

export default App;
