import React, { Component } from 'react';
import './content-section.css';

class ContentSection extends Component {
    render() {
        return (
            <div>
                <div className='content-container'>
                    <div className='content-block'>
                        <div className = 'search-section'>
                            <input className='search-input'></input>
                            <button className='search-button'>Search</button>
                        </div>
                        <div className='result-container'>
                            <code></code>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default ContentSection;
