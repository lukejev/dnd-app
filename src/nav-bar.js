import React, { Component } from 'react';
import './nav-bar.css';

class NavBar extends Component {

    handleClick(e) {
        console.log(e.target)
    }

    render() {
        return (
            <div className='nav-container'>
                <div onClick={this.handleClick} className ='nav-position nav-image'>Starfinder API</div>
                <div onClick={this.handleClick} className ='nav-position nav-item-1'>About</div>
                <div onClick={this.handleClick} className ='nav-position nav-item-2'>Contact</div>
                <div onClick={this.handleClick} className ='nav-position nav-item-3'>Donate</div>
            </div>
        )
    }
}

export default NavBar;
