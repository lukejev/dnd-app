const functions = require('firebase-functions');
const admin = require('firebase-admin');
const express = require('express');
const data = require('./data.json')

admin.initializeApp(functions.config().firebase);
const db = admin.firestore();

const app = express();
const main = express();

const coreRulebook = 'core';
main.use('/api/v1', app);

//test func
app.get('/test', (req, res) => {
    res.set('Cache-Control', 'public, max-age=300, s-maxage=600')
    res.send('This is Starminder\'s test path, everything appears to be working!')
});

// test getter
app.get('/core/classes', (req, res) => {
    db.collection('core').doc('classes').get()
        .then(doc => {
            if (!doc.exists) console.log('document does not exist!test');
            res.send(doc.data());
        })
        .catch(err => {
            console.error('Error getting document', err)
            process.exit();
        })
});

// upload code
if (data && (typeof data === "object")) {
    Object.keys(data).forEach(docKey => {
        db
            .collection(coreRulebook)
            .doc(docKey)
            .set(data[docKey])
            .then((res) => {
                console.log("Document " + docKey + " successfully written!");
            })
            .catch((error) => {
                console.error("Error writing document: ", error);
            });
    });
}

exports.starminderApi = functions.https.onRequest(main);
